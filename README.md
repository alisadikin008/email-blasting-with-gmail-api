# simple FastAPI (Python) with Docker Compose



## Disclaimer

- This is just a very basic package, no DB connection (test data using excel file)
- FastAPI framework, high performance, easy to learn, fast to code, ready for production

## Cloning The Repository


```
git@gitlab.com:alisadikin008/email-blasting-with-gmail-api.git (make sure you have ssh access to gitlab)
```

## Building the Docker Image


```
cd existing_repo (make sure you have docker in your machine)
docker-compose build
```

## Running the Application

```
cd existing_repo (make sure you have docker in your machine)
docker-compose up -d
```

## View in your http engine (browsers, postman, etc)

```
http://localhost:8002
```

## View Interactive API Documentation your http engine (browsers)

```
http://localhost:8008/docs
```

## View Alternative API Documentation your http engine (browsers)

```
http://localhost:8008/redoc
```

## Execute using python command

```
(enter to docker terminal and hit command bellow)
python /code/main.py
```

## `VERY IMPORTANT!!!`

```
- Make sure you have created project in Google and enable API

```


## `DIRECT Blast Sending Email!!!`

```
- change app/configuration/credential.json.sample to credential.json with your own file
- change your mailist.xlsx in app/gmail with your own using existing template
- replace your Curriculum Vitae.pdf in app/gmail with your own data (make sure the file name is Curriculum Vitae.pdf)
go to current repository
cd app/gmail
-change file send.py in line 13 and change the parameter (x,y) => x is start from row in file and y is end of rows
python send.py
```
