def getData(filePath: str,fromRow:int = 1, toRow:int = 1):
    sheet_ranges = filePath['September']
    dataList = []
    for row in range(fromRow,toRow+1):
        Newposition = "".join(sheet_ranges["C" + str(row)].value),
        position = ''.join(Newposition)
        Newcompany = sheet_ranges["D" + str(row)].value,
        company = ''.join(Newcompany)
        data_dict = {
            "date": sheet_ranges["B" + str(row)].value,
            "subject": sheet_ranges["F" + str(row)].value,
            "company": sheet_ranges["D" + str(row)].value,
            "to": sheet_ranges["E" + str(row)].value,
            "body": (
                """\
            <!DOCTYPE html>
            <html>
                <head>
                    <title>Job Application</title>
                </head>
                <body>
                    Dear Hiring Manager,
                    
                    <p>
                        I am writing to express my interest in the <b>{position}</b> role at <b>{company}</b> . With more than 7 years of experience in programming and management, I believe I possess the necessary skills and expertise to excel in this role.
                    </p>
                    
                    <p>
                        My diverse background has allowed me to develop strong technical and organizational skills and the ability to work well in a team. As a computer science graduate, I have gained valuable experience working with people from all walks of life, and I am confident that my skills will be an asset to your team.
                    </p>
                    
                    <p>
                        I am excited about the opportunity to bring my varied experience and make a seamless career change. I am confident my skills and experience align with the requirements of this role, and I am eager to contribute to your team.
                    </p>
                    
                    <p>Thank you for considering my application. I look forward to discussing my qualifications further.</p>
                    
                    <p>Sincerely,<br>Ali Sadikin</p>
                </body>
            </html> 
        """.format(position=position,company=company)
            )
        }
        dataList.append(data_dict)

    return dataList
    