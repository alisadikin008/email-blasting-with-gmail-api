from Google import Create_Service
import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from openpyxl import load_workbook
import sys
sys.path.insert(0, '..')
from data.convert import getData
filePath = load_workbook(filename = 'mailist.xlsx')

data = getData(filePath, 2,11)

CLIENT_SECRET_FILE = '../configuration/credentials.json'
API_NAME = 'gmail'
API_VERSION = 'v1'
SCOPES = ['https://mail.google.com/']

service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

for dt in data:
    mimeMessage = MIMEMultipart()
    mimeMessage['to'] = dt["to"]
    mimeMessage['subject'] = dt["subject"]
    emailMsg = dt["body"]
    mimeMessage.attach(MIMEText(emailMsg, 'html'))
    
    #start attachment
    filePath = "Curriculum Vitae.pdf"
    # Open PDF file in binary mode
    with open(filePath, "rb") as attachment:
        # Add file as application/octet-stream
        # Email client can usually download this automatically as attachment
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email    
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {filePath}",
    )

    # Add attachment to message and convert message to string
    mimeMessage.attach(part)
    raw_string = base64.urlsafe_b64encode(mimeMessage.as_bytes()).decode()
    message = service.users().messages().send(userId='me', body={'raw': raw_string}).execute()
    print(message)